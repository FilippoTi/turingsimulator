/**
    Autore: Tiberio Filippo
    Data di inizio: 22 settembre 2018
    Programma: Simulatore Macchina di Turing

    Versione:
        -1.0 : Creazione Programma Di Base

        -1.1 : Implementazione Grafica e Compilazione Condizionata
        -1.1.1 : Correzione errore riporto Windows

        -1.2.0: Inserimento istruzioni da console sia da Windows che da Linux

        -2.0.0: Termine

*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

////////////////////////////////////////////////////////////////////////

char Nastro[3000]; //Nastro
int StatoMacchina = 0; //Stato Attuale

//Array Paralleli
int StatoAttuale[3000];
int StatoFuturo[3000];
char Movimento[3000];
char CarattereAttuale[3000];
char CarattereFuturo[3000];

//Letture Da file
char PrimaRigaLetta[3000];
char AltreRigheLette[3000];

char StringaAusiliaria[3000];

int PosizioneSulNastro;

int QuanteIstruzioni = 0;

////////////////////////////////////////////////////////////////////////

#if _WIN32

    double speed = 500.0;

    #include <windows.h>
    #include <conio.h>

#elif linux

    double speed = 500000.0;

#endif // _WIN32

////////////////////////////////////////////////////////////////////////

int ripeti = 0;

FILE *fr = NULL;
////////////////////////////////////////////////////////////////////////
<<<<<<< HEAD

#if _WIN32
void SetColor(short color);
#endif // _WIN32

void pulisci();

int inizializzazione();

int esecuzione();

int LetturaDaFile();

int Istruzionimanuali();

void impostazioni();

void intro();

void crediti();

void manuale();

////////////////////////////////////////////////////////////////////////

int main()
{
    int Scelta;
    //Pulizia iniziale dello schermo

    pulisci();

    if(ripeti!=1) //Controllo se � la prima volta che avvio il programma
    {
        intro();
    }

    ripeti = 1; //Setto il check

    while(1==1)
    {
        do //Propongo la scelta
        {
            printf("\033[0;92m");

            printf("\n\n\n    Inserisci 0 per inserire le istruzioni da file");
            printf("\n    Inserisci 1 per inserire le istruzioni manualmente!");
            printf("\n    Inserisci 2 per le impostazioni");
            printf("\n    Inserisci 3 per leggere il manuale");
            printf("\n    Inserisci 4 per terminare\n\n    Inserimento: ");
            scanf("%d", &Scelta);
            if(Scelta<0||Scelta>4)
            {
#if _WIN32
                system("cls");
                SetColor(4);
                printf("\n    VALORE NON VALIDO!!!\n\n");
                SetColor(15);
#elif linux
                system("clear");
                printf("\033[30;40;1m\n   VALORE NON VALIDO!!! \n\n\033[0m");
#endif // _WIN32
            }
        }
        while(Scelta<0||Scelta>4);

        if(inizializzazione() == 1) //Inizializzo tutto
        {
            return -7;
        }
=======
>>>>>>> 59da3c4213d141c766c2f853e9cd397174032c38

        if(Scelta == 0) //Controllo la scelta
        {
            if(LetturaDaFile()==1)  //Controllo se il file viene aperto correttamente
            {
                return -7;
            }
            break;
        }
        else if(Scelta == 1)
        {
            Istruzionimanuali(); //Istruzioni manuali
            break;
        }
        else if(Scelta == 2)
        {
            impostazioni();
        }
        else if(Scelta == 3)
        {
            manuale();
        }
        else if(Scelta == 4)
        {
            fflush(stdout);
            printf("\n\n    Terminato\n---------------------");
            return 0;
        }
        else
        {
#if _WIN32
<<<<<<< HEAD
            SetColor(9);
            printf("ERRORE SCONOSCIUTO!!!");
            SetColor(1);
#elif linux
            printf("\033[30;40;1m ERRORE SCONOSCIUTO!!! \033[0m");
#endif // _WIN32
        }
    }

    if(esecuzione() == 2)
    {
        fflush(stdout);
        printf("\n\n    Terminato\n---------------------");
    }

=======
void SetColor(short color);
#endif // _WIN32

void pulisci();

int inizializzazione();

int esecuzione();

int LetturaDaFile();

int Istruzionimanuali();

void impostazioni();

void intro();

void crediti();

void manuale();

////////////////////////////////////////////////////////////////////////

int main()
{
    int Scelta;
    //Pulizia iniziale dello schermo

    pulisci();

    if(ripeti!=1) //Controllo se � la prima volta che avvio il programma
    {
        intro();
    }

    ripeti = 1; //Setto il check

    while(1==1)
    {
        do //Propongo la scelta
        {
            printf("\033[0;92m");

            printf("\n\n\n    Inserisci 0 per inserire le istruzioni da file");
            printf("\n    Inserisci 1 per inserire le istruzioni manualmente!");
            printf("\n    Inserisci 2 per le impostazioni");
            printf("\n    Inserisci 3 per leggere il manuale");
            printf("\n    Inserisci 4 per terminare\n\n    Inserimento: ");
            scanf("%d", &Scelta);
            if(Scelta<0||Scelta>4)
            {
#if _WIN32
                system("cls");
                SetColor(4);
                printf("\n    VALORE NON VALIDO!!!\n\n");
                SetColor(15);
#elif linux
                system("clear");
                printf("\033[30;40;1m\n   VALORE NON VALIDO!!! \n\n\033[0m");
#endif // _WIN32
            }
        }
        while(Scelta<0||Scelta>4);

        if(inizializzazione() == 1) //Inizializzo tutto
        {
            return -7;
        }

        if(Scelta == 0) //Controllo la scelta
        {
            if(LetturaDaFile()==1)  //Controllo se il file viene aperto correttamente
            {
                return -7;
            }
            break;
        }
        else if(Scelta == 1)
        {
            Istruzionimanuali(); //Istruzioni manuali
            break;
        }
        else if(Scelta == 2)
        {
            impostazioni();
        }
        else if(Scelta == 3)
        {
            manuale();
        }
        else if(Scelta == 4)
        {
            fflush(stdout);
            printf("\n\n    Terminato\n---------------------");
            return 0;
        }
        else
        {
#if _WIN32
            SetColor(9);
            printf("ERRORE SCONOSCIUTO!!!");
            SetColor(1);
#elif linux
            printf("\033[30;40;1m ERRORE SCONOSCIUTO!!! \033[0m");
#endif // _WIN32
        }
    }

    if(esecuzione() == 2)
    {
        fflush(stdout);
        printf("\n\n    Terminato\n---------------------");
    }

>>>>>>> 59da3c4213d141c766c2f853e9cd397174032c38
    return 0;
}

////////////////////////////////////////////////////////////////////////

#if _WIN32
void SetColor(short color)
{
    HANDLE hCon = GetStdHandle(STD_OUTPUT_HANDLE);
    SetConsoleTextAttribute(hCon,color);
}
#endif // _WIN32

////////////////////////////////////////////////////////////////////////

void pulisci()
{
#if _WIN32
    system("cls");

#elif linux
    system("clear");
#endif // _WIN32
}

////////////////////////////////////////////////////////////////////////

int inizializzazione()
{
    //Inizializzo
    int i;

    for(i=0; i<2998; i++)//Ciclo di inizializzazione
    {
        Nastro[i]=32;
        StatoAttuale[i]=-1;
        StatoFuturo[i]=0;
        CarattereFuturo[i]=32;
        CarattereAttuale[i]=32;
    }
    Nastro[2998]='\n';//Terminatore

    StatoMacchina=0;
    PosizioneSulNastro=3000/2;

    return 0;
}

////////////////////////////////////////////////////////////////////////

int esecuzione()
{
    int ContatorePerLeStampe; //Variabili
    int IndiceRicerca=0;
    int VariabileDiControlloPerGliStati=0;
    int j;

    int ContaIstruzioni = 0;

    while(VariabileDiControlloPerGliStati!=1)
    {
        pulisci();
        fflush(stdout);

////////////////////////////////////////////////////////////////////////

        printf("\n           ");    //Stampa del nastro
        for(ContatorePerLeStampe=0; ContatorePerLeStampe<3000; ContatorePerLeStampe++)
        {
#if _WIN32
            if(Nastro[ContatorePerLeStampe]!=32)
            {

                if(ContatorePerLeStampe==PosizioneSulNastro)
                {
                    SetColor(6);
                }
                printf("%c ", Nastro[ContatorePerLeStampe]);
            }
            SetColor(7);


            fflush(stdout);
#elif linux
            if(Nastro[ContatorePerLeStampe]!=32)
            {
                if(ContatorePerLeStampe==PosizioneSulNastro)
                {
                    printf("\033[31;37;1m%c \033[0m", Nastro[ContatorePerLeStampe]);
                }
                else
                {
                    printf("%c ", Nastro[ContatorePerLeStampe]);
                }
            }

            fflush(stdout);
#endif // _WIN32
        }

////////////////////////////////////////////////////////////////////////

        printf("\n         ");  //Stampa della freccia

        if(PosizioneSulNastro>=1500)
        {
            for(j=0; j<=PosizioneSulNastro-1500; j++)
            {
                printf("  ");

            }
        }


        fflush(stdout);

        printf("^");
        printf("\n         ");


        fflush(stdout);

        if(PosizioneSulNastro>=1500)
        {
            for(j=0; j<=PosizioneSulNastro-1500; j++)
            {
                printf("  ");

            }
        }


        fflush(stdout);

        printf("|\n");
        fflush(stdout);

////////////////////////////////////////////////////////////////////////

        VariabileDiControlloPerGliStati=1; //Calcolo del simulatore
        IndiceRicerca=0;

        while(StatoAttuale[IndiceRicerca]!=-1) //Finch� non trovo i terminatori degli stati
        {
            if(StatoMacchina==StatoAttuale[IndiceRicerca]&&Nastro[PosizioneSulNastro]==CarattereAttuale[IndiceRicerca]) //Se trovo stato e char
            {
                StatoMacchina=StatoFuturo[IndiceRicerca];  //Imposto stato e nastro
                Nastro[PosizioneSulNastro]=CarattereFuturo[IndiceRicerca];

                if(Movimento[IndiceRicerca]=='>') //Controllo i movimenti
                {
                    PosizioneSulNastro++;
                }
                else if(Movimento[IndiceRicerca]=='-')
                {

                }
                else if(Movimento[IndiceRicerca] == '<')
                {
                    PosizioneSulNastro--;
                }

                VariabileDiControlloPerGliStati = 0; //Setto il controllo
                break;
            }

            IndiceRicerca++;
        }

////////////////////////////////////////////////////////////////////////

        ContaIstruzioni = 0; //Stampa istruzioni

        while(StatoAttuale[ContaIstruzioni] != -1)
        {
            if(ContaIstruzioni == IndiceRicerca)
            {
#if _WIN32
                SetColor(6);
#elif linux
                printf("\033[1;91m");
#endif // _WIN32
            }
            printf("    (%d,", StatoAttuale[ContaIstruzioni]);
            printf(" %c,", CarattereAttuale[ContaIstruzioni]);
            printf(" %d,", StatoFuturo[ContaIstruzioni]);
            printf(" %c,", CarattereFuturo[ContaIstruzioni]);
            printf(" %c)    ", Movimento[ContaIstruzioni]);

            if((ContaIstruzioni + 1) % 3 == 0 )
            {
                printf("\n");
            }
#if _WIN32
            SetColor(7);
#elif linux
            printf("\033[0;37m");
#endif // _WIN32

            ContaIstruzioni++;
            fflush(stdout);

        }

////////////////////////////////////////////////////////////////////////

#if _WIN32  //Delay
        _sleep(speed);
#elif linux
        usleep((int) speed);
#endif // _WIN32
        fflush(stdout);

    }

    return 2;
}

////////////////////////////////////////////////////////////////////////

int LetturaDaFile()
{
    int i;  //Inizializzazione
    fr = fopen("istruzioni.txt", "r");
    int contatore = 0;

    if( fr == NULL) //Controllo l'apertura
    {
        pulisci();
#if linux
        printf("\033[30;91;1m\n\n\n    Errore nel aprire istruzioni.txt\n\n\033[0m");
#elif _WIN32
        printf("\n\n\n    Errore nel aprire istruzioni.txt\n\n");
#endif // linux

        fflush(stdout);

        FILE *Cf;

        Cf = fopen("istruzioni.txt", "w");

        fclose(Cf);


        return 1;
    }

////////////////////////////////////////////////////////////////////////

    fgets( StringaAusiliaria, sizeof(StringaAusiliaria), fr); //Setto il nastro

    for(i=0; i<=strlen(StringaAusiliaria); i++)
    {
        if(StringaAusiliaria[i]!='\n'&&StringaAusiliaria[i]!='\r')
        {
            Nastro[PosizioneSulNastro+i]=StringaAusiliaria[i];
        }
    }

//////////////////////////////////////////////////////////////////////// Array

    while(fgets( StringaAusiliaria, sizeof(StringaAusiliaria), fr)!= NULL)
    {
        StatoAttuale[contatore]= StringaAusiliaria[1]-48; //Stato attuale

        CarattereAttuale[contatore] = StringaAusiliaria[3]; //Char attuale

        if(CarattereAttuale[contatore]=='#')
        {
            CarattereAttuale[contatore]=32;
        }

        StatoFuturo[contatore] = StringaAusiliaria[5]-48; //Stato futuro

        CarattereFuturo[contatore] = StringaAusiliaria[7]; //Char futuro
        if(CarattereFuturo[contatore]=='#')
        {
            CarattereFuturo[contatore]=32;
        }

        Movimento[contatore] = StringaAusiliaria[9]; //Movimento

        contatore++;
    }
    StatoAttuale[contatore] = -1;
    return 0;
}

////////////////////////////////////////////////////////////////////////

int Istruzionimanuali()
{
    //VAR
    int i;
    int contatore = 0;
    int chiusura = 0;
    int count = 0;

    getchar();

    pulisci();

////////////////////////////////////////////////////////////////////////

    //Nastro;
    printf("\n\n      Inserisci i caratteri da inserire nel nastro (Per esempio 100 poi invio)\n         Inserimento: ");
    fgets(StringaAusiliaria, sizeof(StringaAusiliaria), stdin);

    for(i=0; i<strlen(StringaAusiliaria)-1; i++)
    {
        Nastro[PosizioneSulNastro+i]=StringaAusiliaria[i];
    }
    //End Nastro;

//////////////////////////////////////////////////////////////////////// Array

    printf("\n      Scrivi le istruzioni, per terminarle digitare\n      nello 'statoAttuale' il numero -1\n");

    while(1==1)
    {
        do      //Stato attuale
        {
            printf("\n      Inserisci lo stato attuale (-1 per terminare)\n         Inserimento: ");

            scanf("%d", &StatoAttuale[count]);

            if(StatoAttuale[count]==-1)
            {
                chiusura=1;
                break;
            }
            if(StatoAttuale[count] < -1)
            {
                printf("\n      Valore non valido!");
            }
        }
        while(StatoAttuale[count] < -1);

        if(chiusura == 1)
        {
            break;
        }
        getchar();

////////////////////////////////////////////////////////////////////////////////////////

        do
        {
            printf("\n      Inserisci il carattere letto\n         Inserimento: ");
            CarattereAttuale[count] = getchar();
            if(CarattereAttuale[count] == '\n')
            {
                printf("\n      Valore non valido!");
            }
        }
        while(CarattereAttuale[count] == '\n');


        if(CarattereAttuale[count] == '#')
        {
            CarattereAttuale[count]=' ';
        }
        getchar();

////////////////////////////////////////////////////////////////////////////////////////

        do
        {
            printf("\n      Inserisci lo stato futuro\n         Inserimento: ");
            scanf("%d", &StatoFuturo[count]);

            if(StatoAttuale[count] < 0)
            {
                printf("\n      Valore non valido!");
            }
        }
        while(StatoFuturo[count] < 0);
        getchar();

////////////////////////////////////////////////////////////////////////////////////////

        do
        {
            printf("\n      Inserisci il carattere da scrivere\n         Inserimento: ");
            CarattereFuturo[count] = getchar();
            if(CarattereFuturo[count] == '\n')
            {
                printf("\n      Valore non valido!");
            }
        }
        while(CarattereFuturo[count] == '\n');

        if(CarattereFuturo[count] == '#')
        {
            CarattereFuturo[count]=32;
        }
        getchar();

////////////////////////////////////////////////////////////////////////////////////////

        do
        {
            printf("\n    Inserisci il movimento da effettuare (> dx, - fermo, < sx)\n         Inserimento: ");
            scanf("%c", &Movimento[count]);
            getchar();

            printf("%c",Movimento[count] );

            if(Movimento[count] != '<' && Movimento[count] != '-' && Movimento[count] != '>')
            {
                printf("\n      Valore non valido!");
            }
        }
        while(Movimento[count] != '<' && Movimento[count] != '-' && Movimento[count] != '>');

////////////////////////////////////////////////////////////////////////////////////////

        fflush(stdout);

#if _WIN32
        _sleep(400);
        system("cls");
#elif linux
        usleep(400000);
        system("clear");
#endif // _WIN32

        fflush(stdout);

        contatore++;
        count++;
    }

    return 0;
}

////////////////////////////////////////////////////////////////////////

void impostazioni()
{
    int sceltaInterna;

    while(1==1)
    {
        pulisci();

        do //Propongo la scelta
        {
            printf("\n\n\n    Inserisci 0 per settare la velocita del simulatore\n");
            printf("    Inserisci 1 per leggere i crediti\n");
            printf("    Inserisci 2 per uscire dalle impostazioni\n    Inserimento: ");

            scanf("%d", &sceltaInterna);

            if(sceltaInterna<0||sceltaInterna>3)
            {
#if _WIN32
                system("cls");
                SetColor(4);
                printf("\n    VALORE NON VALIDO!!!\n\n");
                SetColor(15);
#elif linux
                system("clear");
                printf("\033[30;40;1m\n   VALORE NON VALIDO!!! \n\n\033[0m");
#endif // _WIN32
            }
        }
        while(sceltaInterna<0||sceltaInterna>3);

        if(sceltaInterna == 0)
        {
            pulisci();
#if _WIN32
            printf("\n\n\n      Velocita attuale: %.1lf \n", speed/1000.0);
#elif linux
            printf("\n\n\n      Velocita attuale: %.1lf \n", speed/1000000.0);
#endif // _WIN32
            do
            {
                printf("      Inserisci una velocita' tra 0.1 e 5.0 (5.0 sta per lento e 0.1 per veloce)\n        Inserimento: ");
                scanf("%lf", &speed);
            }
            while(speed<0.1 || speed >5.0);

#if _WIN32
            speed *= 1000;
#elif linux
            speed *= 1000000;
            //SpeedLinux = (int) speed;
#endif // _WIN32

        }
        else if(sceltaInterna == 1)
        {
            pulisci();
            crediti();
        }
        else if(sceltaInterna == 2)
        {
            pulisci();
            return;
        }
        else
        {
#if _WIN32
            SetColor(9);
            printf("ERRORE SCONOSCIUTO!!!");
            SetColor(1);
#elif linux
            printf("\033[30;40;1m ERRORE SCONOSCIUTO!!! \033[0m");
#endif // _WIN32
            return;
        }

    }
}

////////////////////////////////////////////////////////////////////////

void intro()
{
#if linux
    pulisci();
    printf("\033[0;92m");



    printf("_______         _                 _____ _                 _       _             \n");
    printf("__   __|       (_)               / ____(_)               | |     | |            \n");
    printf("  | | _   _ _ __ _ _ __   __ _  | (___  _ _ __ ____ _   _| | __ _| |_ ___  _ __ \n");
    printf("  | || | | | '__| | '_  |/ _` |  |___ || | '_ ` _  | | | | |/ _` | __/ _ || '__|\n");
    printf("  | || |_| | |  | | | | | (_| |  ____) | | | | | | | |_| | | (_| | || (_) | |   \n");
    printf("  |_|| __,_|_|  |_|_| |_||__, | |_____/|_|_| |_| |_||__,_|_||__,_|___|___/|_|   \n");
    printf("                          __/ |                                                 \n");
    printf("                         |___/                                                  \n");

    fflush(stdout);
    usleep(1500000);
    pulisci();

    printf("\033[0;92m");

    printf("\n\n      Sistema Utilizzato : Linux");
    printf("\n\n      Attenzione!!!");
    printf("\n\n      In questa versione non ci sono contolli per verificare che le istruzioni\n      siano giuste!");

    fflush(stdout);
    usleep(1500000);
    pulisci();

    printf("\033[0;37m");
    fflush(stdout);

////////////////////////////////////////////////////////////////////////

#elif _WIN32

    SetColor(2);

    printf(" _______         _                 _____ _                 _       _             \n");
    printf("|__   __|       (_)               / ____(_)               | |     | |            \n");
    printf("   | | _   _ _ __ _ _ __   __ _  | (___  _ _ __ ____ _   _| | __ _| |_ ___  _ __ \n");
    printf("   | || | | | '__| | '_  |/ _` |  |___ || | '_ ` _  | | | | |/ _` | __/ _ || '__|\n");
    printf("   | || |_| | |  | | | | | (_| |  ____) | | | | | | | |_| | | (_| | || (_) | |   \n");
    printf("   |_|| __,_|_|  |_|_| |_||__, | |_____/|_|_| |_| |_||__,_|_||__,_|___|___/|_|   \n");
    printf("                           __/ |                                                 \n");
    printf("                          |___/                                                  \n");

    fflush(stdout);
    _sleep(1500);
    pulisci();



    printf("\n\n      Sistema Utilizzato : Windows");
    printf("\n\n      Attenzione!!!");
    printf("\n\n      In questa versione non ci sono contolli per verificare che le istruzioni\n      siano giuste!");

    fflush(stdout);
    _sleep(1500);
    pulisci();

    SetColor(15);

#endif // linux
}

////////////////////////////////////////////////////////////////////////

void crediti()
{
    printf("\n    Autore: Tiberio Filippo 3Ai www.filippotiberio.it\n");
    printf("\n    Data di inizio: 22 settembre 2018");
    printf("\n    Programma: Simulatore Macchina di Turing");
    printf("\n    Versioni:\n        -1.0.0 : Creazione Programma Di Base\n");
    printf("\n        -1.1.0 : Implementazione Grafica e Compilazione Condizionata\n");
    printf("\n        -1.1.1 : Correzione errore riporto Windows\n");
    printf("\n        -1.2.0 : Inserimento istruzioni da console sia da Windows che da Linux\n");
    printf("\n        -2.0.1 : Termine del programma con pseudo grafica\n");
    printf("\n\n    Premi un tasto qualunque per uscire...\n");
    getchar();
    getchar();
}

////////////////////////////////////////////////////////////////////////

void manuale()
{
    int scelta;
    while(1==1)
    {
        pulisci();
        do
        {
            printf("\n\n    Inserisci 1 per il manuale riguardo l'esecuzione da file");
            printf("\n    Inserisci 2 per il manuale riguardo l'inserimento manuale");
            printf("\n    Inserisci 3 per il manuale riguardo le impostazioni");
            printf("\n    Inserisci 4 per il manuale riguardo la sintassi delle istruzioni");
            printf("\n    Inserisci 0 per uscire\n\n    Inserimento: ");
            scanf("%d", &scelta);

            if(scelta < 0 || scelta > 4)
            {
                pulisci();
                printf("\n    Valore non valido!\n");
            }
        }
        while(scelta < 0 || scelta > 4);

        if(scelta == 0)
        {
            pulisci();
            break;
        }
        else if(scelta == 1)
        {
            pulisci();
            printf("\n\n\n    -Il file deve esistere altrimenti il programma teminera' \n     restituendo un errore e creando il file\n");
            printf("\n    -La prima riga deve contenere i caratteri da inserire nel nastro\n");
            printf("\n    -Le successive devono contenere le istruzioni, una per riga e\n     secondo la corretta sintassi\n");
            printf("\n\n    -Premere un tasto qualsiasi per continuare ... ");

            getchar();
            getchar();
        }
        else if(scelta == 2)
        {
            pulisci();
            printf("\n\n\n    -Durante l'inserimento manuale, vi verra' prima di tutto chiesto");
            printf("\n     di inserire i caratteri nel nastro\n");
            printf("\n    -Successivamente dovrete inserire le istruzioni seguendo la precisa sintassi");
            printf("\n     Per terminare inserire -1 nello stato attuale\n");
            printf("\n\n    -Premere un tasto qualsiasi per continuare ... ");

            getchar();
            getchar();
        }
        else if(scelta == 3)
        {
            pulisci();
            printf("\n\n\n    -Nel menu delle impostazioni e' possibile: \n");
            printf("\n     1- Impostare la velocita' del simulatore con un valore\n");
            printf("         compreso tra 0.1(Veloce) e 5.0(Lentissimo) \n");
            printf("\n     2- Visualizzare i crediti\n");
            printf("\n\n    -Premere un tasto qualsiasi per continuare ... ");

            getchar();
            getchar();
        }
        else if(scelta == 4)
        {
            pulisci();
            printf("\n\n\n    La corretta sintassi da usare in questo simulatore e': \n");
            printf("\n    -(StatoAttuale,CarattereAttuale,StatoFuturo,CarattereFuturo,Movimento)\n");
            printf("\n    -Per inserire o leggere uno spazio vuoto usare il siblolo '#'\n");
            printf("      per esempio -> (0,#,1,a,>): se trova uno spazio vuoto scrive 'a'\n");
            printf("\n    -Per i movimenti: < = Sinistra; - = Fermo; > = Destra\n");
            printf("\n    -In questa versione le istruzioni non vengono controllate\n");


            printf("\n\n    -Premere un tasto qualsiasi per continuare ... ");

            getchar();
            getchar();
        }
        else
        {
            printf("\n      Errore!!!!");
        }

        pulisci();
    }
}
////////////////////////////////////////////////////////////////////////
<<<<<<< HEAD
=======

>>>>>>> 59da3c4213d141c766c2f853e9cd397174032c38
